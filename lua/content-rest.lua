local xml2lua = require("xml2lua")
local handler = require("xmlhandler.tree")
local cjson = require "cjson"
local httpc = require("resty.http").new()
local zlib = require "zlib"
local api_url = os.getenv("API_URL")
local api_path = os.getenv("API_PATH")

local function isempty(s)
    return s == nil or s == ''
end

function compress(str)
    local level = 5
    local windowSize = 15 + 16
    return zlib.deflate(level, windowSize)(str, "finish")
end

local uri_str = api_url .. "/" .. api_path .. "/" .. ngx.var.path
local method_name = ngx.req.get_method()
local args = ngx.req.get_uri_args()
local post_data = ngx.req.get_body_data()
local json_data = ""
local headers_tmp = ngx.req.get_headers()

if (post_data) then
    handler = handler:new()
    local parser = xml2lua.parser(handler)
    parser:parse(post_data)
    json_data = cjson.encode(handler.root)
    headers_tmp["content-length"] = #json_data
    headers_tmp["content-type"] = "application/json"
    --if ngx.req.get_method() == "POST" then
    --    for k, v in pairs(headers_tmp) do
    --        ngx.log(ngx.ERR, "k:v ", k .. " : " .. v)
    --    end
    --end
end

local res, err = httpc:request_uri(uri_str, {
    method = method_name,
    query = args,
    body = json_data,
    headers = headers_tmp,
})
if not res then
    ngx.log(ngx.ERR, "request failed: ", err)
    return
end

local status = res.status
if not isempty(res.body) then
    local xmldata = xml2lua.toXml(cjson.decode(res.body))
    local body = compress(xmldata)
    ngx.ctx.content_length = #body
    ngx.print(body)
end
