local xml2lua = require("xml2lua")
local cjson = require "cjson"

local function isempty(s)
  return s == nil or s == ''
end

if not isempty(ngx.arg[1]) then
    ngx.arg[1] = xml2lua.toXml(cjson.decode(ngx.arg[1]))
end

ngx.arg[2] = true
